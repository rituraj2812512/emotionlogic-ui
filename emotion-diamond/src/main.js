import {createApp} from 'vue'
import '@/assets/styles/reset.scss'
import en from './locales/en.json'
import App from './App.vue'
import {registerPlayer} from './web-components/index.js'
registerPlayer()

import { createI18n } from 'vue-i18n'

const i18n = createI18n({
  legacy: false, // If you're using Vue 3.2 or later
  locale: 'en',
  messages: {
    en,
  },
})

const app = createApp(App)
app.use(i18n)
app.mount('#app')
