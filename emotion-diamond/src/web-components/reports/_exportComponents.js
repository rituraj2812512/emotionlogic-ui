import { defineCustomElement } from 'vue'

import _AnalysisReport from './AnalysisReport.ce.vue'
import _AnalysisChannelReport from './AnalysisChannelReport.ce.vue'

export const NMSAnalysisReport = defineCustomElement(_AnalysisReport)
export const NMSAnalysisChannelReport = defineCustomElement(_AnalysisChannelReport)