import * as Components from './_exportComponents.js'

export function registerNMSAnalysisReport(htmlTag = 'nms-analysis-report') {
    customElements.define(htmlTag, Components.NMSAnalysisReport)
}
export function registerNMSAnalysisChannelReport(htmlTag = 'nms-analysis-channel-report') {
    customElements.define(htmlTag, Components.NMSAnalysisChannelReport)
}