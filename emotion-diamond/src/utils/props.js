export const parseProps = (props, defaultValues = {}) => {
    const dv = defaultValues;
    return Object.entries(props).reduce((acc, [key, value]) => {
        acc[key] = (typeof value === 'object' ? JSON.stringify(value) : value) || dv[key]
        return acc
    }, {})
}
