window.registerNMSDiamond()
window.registerLVA()
window.registerTimeCode()
window.registerEmotionalStyleLVA()
window.registerVideoPlayer()

const analyzeForm = document.getElementById('analyzeForm')

analyzeForm.addEventListener('submit', (e) => {
    e.preventDefault()
    document.querySelector('.blink-me').classList.add('show')
    const fd = new FormData(analyzeForm)
    fd.append('consentObtainedFromDataSubject', true)

    function previewFile(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.addEventListener(
                'load',
                () => {
                    // convert image file to base64 string
                    resolve(reader.result)
                },
                false,
            )

            if (file) {
                reader.readAsDataURL(file)
            }
        })
    }

    const showPlayer = (sample) => {
        document.getElementById('firstStep').style.display = 'none'
        document.getElementById('secondStep').style.display = 'block'

        const convertedData = window.nmsDataConverter(sample.data)
        const finallyContent = convertedData.finallyContent
        const timeCodesOnVideoLine = convertedData.timeCodesOnVideoLine

        const videoPlayer = document.querySelector('nms-video-player')
        videoPlayer.setAttribute('video-url', sample.url)
        videoPlayer.setAttribute('time-codes', JSON.stringify(timeCodesOnVideoLine))

        const videoTag = videoPlayer._instance.refs.videoRef

        const lvaEmotinalStyleHTML = document.querySelector('#lvaEmotinalStyle')
        const diamondHTML = document.querySelector('#diamond')
        const lvaTimeCodeHTML = document.querySelector('#lvaTimeCode')
        const lvaOnlineHTML = document.querySelector('#lvaOnline')

        videoTag.addEventListener('timeupdate', (e) => {
            const currentTime = Math.ceil(e.target.currentTime)
            if (String(currentTime) in finallyContent) {
                const curData = finallyContent[String(currentTime)]
                diamondHTML.setAttribute('values', JSON.stringify(curData.diamond))
                lvaOnlineHTML.setAttribute(
                    'lva-status',
                    JSON.stringify(curData.lvaStatus),
                )

                if(curData.timeCode){
                    lvaTimeCodeHTML.setAttribute(
                        'time-code',
                        JSON.stringify(curData.timeCode),
                    )
                    lvaTimeCodeHTML.setAttribute('animation-width', JSON.stringify(true))
                }
                lvaEmotinalStyleHTML.setAttribute(
                    'current-emotional-style',
                    curData.currentEmotionalStyle,
                )
            }
        })
    }

    fetch('https://cloud.emlo.cloud/analysis/analyzeFile', {
        method: 'POST',
        body: fd,
    })
        .then((r) => r.json())
        .then((data) => {
            if (data && data.success) {
                previewFile(document.getElementById('file').files[0]).then((url) => {
                    showPlayer({
                        data: data.data,
                        url: url,
                    })
                })
            } else if (data && !data.success) {
                alert(data.error)
            }
            document.querySelector('.blink-me').classList.remove('show')
        })
        .catch((e) => {
            document.querySelector('.blink-me').classList.remove('show')
            throw Error(e)
        })
})
