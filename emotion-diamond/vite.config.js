import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import svgLoader from 'vite-svg-loader'
import { createStyleImportPlugin } from 'vite-plugin-style-import'
import { fileURLToPath, URL } from 'url'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: {
        compilerOptions: {
          // treat all tags with a dash as custom elements
          isCustomElement: (tag) => tag.includes('nms-')
        }
      }
    }),
    svgLoader({svgo: false}),
    createStyleImportPlugin({
      scss: {
        // Add the path to your global SCSS file here
        additionalData: `@import "./src/assets/styles/component.scss";`,
      },
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@use "@/assets/styles/component.scss" as *;',
      },
    },
  },
  define: {
    'process.env.NODE_ENV': '"production"',
  },
  build: {
    lib: {
      entry: './src/web-components/index.js',
      formats: ['es', 'cjs', 'umd'],
      name: 'webComponent',
      fileName: (format) => {
        if (format === 'es') return 'nms-lib.js'
        if (format === 'cjs') return 'nms-lib.cjs'
        if (format === 'umd') return 'nms-lib.umd.cjs'
      },
    },
    sourcemap: true,
    // Reduce bloat from legacy polyfills.
    target: 'esnext',
    // Leave minification up to applications.
    minify: false,
  },
})
