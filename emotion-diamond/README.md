#Scripts
_npm i_ - for install

_npm run dev_ - for start in mode development

_npm run build_ - for build Web components

When run _npm i_ command and then _npm run build_

---

# nms-diamond

### Props

values: String
####Example

> JSON.stringify([

        { text: "emotional", value: 53 },
        { text: "uneasy", value: 43 },
        { text: "stressful", value: 73 },
        { text: "thoughtful", value: 63 },
        { text: "confident", value: 33 },
        { text: "concentrated", value: 80 },
        { text: "energetic", value: 67 },
        { text: "passionate", value: 64 },

])

colors: String
####Example

> default: '["#e95711","#ff8528"]'
> JSON.stringify(['red', 'black'])

height: String

> default: '500'
> number in string String(500)

width: String

> default: '700'
> number in string String(700)

max: String
####Example

> default: '100'
> number in string String(100)

### Example in pure js

You should import registerNMSDiamond from … and call this function

```html
<nms-diamond id="diamond"></nms-diamond>

<script type="module">
  import { registerNMSDiamond } from './dist/index.js'
  registerNMSDiamond()

  const diamondHTML = document.querySelector('#diamond')

  diamondHTML.setAttribute('values', JSON.stringify(diamond))
  diamondHTML.setAttribute('colors', JSON.stringify(['#e95711', '#ff8528']))
  diamondHTML.setAttribute('height', JSON.stringify(500))
  diamondHTML.setAttribute('width', JSON.stringify(700))
  diamondHTML.setAttribute('max', JSON.stringify(100))
</script>
```

### Example in react

```jsx
import { useState } from 'react'
import { registerNMSDiamond } from './dist/index.js'

registerNMSDiamond()

const Diamond = () => {
  const [value, setValue] = useState([])
  const colors = []

  return (
    <div>
      <nms-diamond
        values={JSON.stringify(value)}
        colors={JSON.stringify(colors)}
        max={100}
      />
    </div>
  )
}

export default Diamond
```

You can pass other props to the component as in the example

---

# nms-lva-online

### Props

lvaStatus: String (enum number with status)

> type: String
> default: '-100'

animate: String (Boolean in string)

> type: String
> default: ''
> JSON.stringify(true) or JSON.stringify(false)

animateWidthDuration: String (Number in string) in ms

> type: String
> default: ''
> JSON.stringify(1500)

animateShowTextBlock: String (Number in string)

> type: String
> default: '1500'
> JSON.stringify(2000)

lvaTexts: String (enumLVAText) see in utils

> type: String
> default: '{}', // {} as enumLVAText

### Example in pure js

```html
<nms-lva-online id="lvaOnline" animate-width-duration="4500" />

<script type="module">
  import { registerLVA } from './dist/index.js'
  registerLVA()
  const lvaOnlineHTML = document.querySelector('#lvaOnline')
  lvaOnlineHTML.setAttribute('lva-status', JSON.stringify('-1'))
</script>
```

### Example in react js

```js
import { useState } from 'react'
import { registerNMSDiamond, enumLVATexts } from './dist/index.js'

registerNMSDiamond()

const LVAStatus = () => {
  const [lvaStatus, setLVAStatus] = useState(2)
  const animate = true
  const animateWidthDuration = 1500
  const lvaTexts = enumLVATexts

  return (
    <div>
      <nms-lva-online
        lva-status={JSON.stringify(lvaStatus)}
        animate={JSON.stringify(animate)}
        animate-width-duration={JSON.stringify(animateWidthDuration)}
        lva-texts={JSON.stringify(lvaTexts)}
      />
    </div>
  )
}

export default LVAStatus
```

---

# nms-lva-timecode

### Props

> timeCode: String // {time: string, text: string}
> type: String
> default: '',

### Example in pure js

```html
<nms-lva-timecode id="lvaTimeCode" />
<script type="module">
  import { registerTimeCode } from './dist/index.js'
  registerTimeCode()
  const lvaTimeCodeHTML = document.querySelector('#lvaTimeCode')
  lvaTimeCodeHTML.setAttribute(
    'time-code',
    JSON.stringify({
      time: '00:05',
      text: 'Defensive Emotional',
    }),
  )
</script>
```

### Example in react

```js
import { useState } from 'react'
import { registerTimeCode } from './dist/index.js'

registerTimeCode()

const TimeCode = () => {
  const [timeCode, setTimeCode] = useState({}) // {time: string, text: string}

  return <nms-lva-timecode time-code={JSON.stringify(timeCode)} />
}

export default TimeCode
```

---

# nms-lva-emotional-style

### Props

```js
currentEmotionalStyle: {
    type: String
    default: ''
}

animationWidth: {
    type: String
    default: '' // boolean
}
```

### Example in pure js

```html
<nms-lva-emotional-style id="lvaEmotinalStyle" />
<script type="module">
  import { registerEmotionalStyleLVA } from './dist/index.js'
  registerEmotionalStyleLVA()
  const lvaEmotinalStyle = document.querySelector('#lvaEmotinalStyle')
  lvaEmotinalStyle.setAttribute(
    'current-emotional-style',
    'Energetic Emotional',
  )
</script>
```

### Example in react

```js
import { useState } from 'react'
import { registerEmotionalStyleLVA } from './dist/index.js'

registerEmotionalStyleLVA()

const EmotionalStyle = () => {
  const [emloStatus, setEmloStatus] = useState('Defensive Emotional')

  return (
    <div>
      <nms-lva-emotional-style
        current-emotional-style={emloStatus}
        animation-width={JSON.stringify(true)}
      />
    </div>
  )
}

export default EmotionalStyle
```
