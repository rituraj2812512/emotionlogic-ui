﻿export const PlayerStatus = {
    stopped: 1,
    playing: 2,
    paused: 3,
    pauseAfter: 4
}