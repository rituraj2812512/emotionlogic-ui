﻿
function getLine(Locale, name, optionLines, isMultichannel, getDrawItems, disableTooltip = false){
    const result = {
        name: name,
        lineHeight: 10,
        lineItems: [{
            baseColor: optionLines.baseColor,
            invertColors: optionLines.invertColors,
            tooltip: !disableTooltip,
            tooltipTitle: isMultichannel.value ? Locale.value.channel0 : '',
            drawItems: getDrawItems(0),
        }]
    };

    if(isMultichannel.value){
        result.lineHeight = 7;
        result.lineItems.push({
            baseColor: optionLines.baseColor,
            invertColors: optionLines.invertColors,
            tooltip: !disableTooltip,
            tooltipTitle: Locale.value.channel1,
            drawItems: getDrawItems(1),
        })
    }

    return result;
}


export function getRiskLine(Locale, isMultichannel, optionLines, validation = null){
    const getDrawItems = (channel) => ([{
        headerName: 'EmotionPlayer-Risk',
        tooltipName: Locale.value.risk,
        tooltipColor: '#2A385E',
        getColor: (val) => {
            if(val > 100 && val <= 170) return '#FFC700' //val = 127
            if(val > 170 && val <= 220) return '#FF8000' //val = 200
            if(val > 220) return '#FF0000' //val = 255
            return optionLines.baseColor;
        },
        mixColor: false,
        channel: channel,
        validation: validation && validation(channel),
    }]);
    
    return getLine(Locale, Locale.value.risk, optionLines, isMultichannel, getDrawItems)
}

export function getEmotionsLine(Locale, isMultichannel, optionLines, validation = null){
    const getDrawItems = (channel) => ([{
        headerName: 'EmotionPlayer-Sad',
        tooltipName: Locale.value.sadness,
        tooltipColor: '#4E9BF0',
        getColor: () => '#0000FF',
        channel: channel,
        validation: validation && validation(channel),
    },{
        headerName: 'EmotionPlayer-Joy',
        tooltipName: Locale.value.joy,
        tooltipColor: '#8BC541',
        getColor: () => '#00FF00',
        channel: channel,
        validation: validation && validation(channel),
    },{
        headerName: 'EmotionPlayer-Aggression',
        tooltipName: Locale.value.aggression,
        tooltipColor: '#FF0000',
        getColor: () => '#FF0000',
        channel: channel,
        validation: validation && validation(channel),
    }]);

    return getLine(Locale, Locale.value.emotions, optionLines, isMultichannel, getDrawItems)
}


export function getStressLine(Locale, isMultichannel, optionLines, validation = null){
    const getDrawItems = (channel) => ([{
        headerName: 'EmotionPlayer-Stress',
        tooltipName: Locale.value.stress,
        tooltipColor: '#2A385E',
        getColor: () => '#FFFF00',
        channel: channel,
        validation: validation && validation(channel),
    }]);

    return getLine(Locale, Locale.value.stress, optionLines, isMultichannel, getDrawItems)
}

export function getEnergyLine(Locale, isMultichannel, optionLines, validation = null){
    const getDrawItems = (channel) => ([{
        headerName: 'EmotionPlayer-Energy',
        tooltipName: Locale.value.energy,
        tooltipColor: '#2A385E',
        getColor: () => '#FFFFFF',
        channel: channel,
        validation: validation && validation(channel),
    }]);

    return getLine(Locale, Locale.value.energy, optionLines, isMultichannel, getDrawItems)
}

export function getSpeakersLine(Locale, isMultichannel, optionLines, validation = null){
    const getDrawItems = (channel) => ([{
        headerName: 'channel',
        getColor: () => channel === 0 ? '#FFCC99' : '#99CCCC',
        mixColor: false,
        channel: channel,
        validation: validation && validation(channel),
    }]);

    return getLine(Locale, Locale.value.channels, optionLines, isMultichannel, getDrawItems, true)
}