﻿import {ref} from "vue";

const defaultArea = {
    selecting: false,
    clientXFrom: 0,
    from: 0,
    to: 0,
    maxWidth: 0,
}

export const instance = {
   value: newInstance()
};

export function initializeNewInstance(){
    instance.value = newInstance();
}

function newInstance(){
    const instance = {}
    instance.selectedArea = ref({...defaultArea});
    instance.resetSelectedArea = () => {
        instance.selectedArea.value = {...defaultArea};
    };
    instance.selectedAreaPlaying = ref(false);
    instance.selectedAreaContinuePlaying = ref(false);
    instance.currentTooltipSegment = ref(null);
    instance.zoomMultiplier = ref(1);
    instance.playPosition = ref('0%');
    instance.headersPositions = ref({});
    instance.validSegments = ref([]);
    instance.calculatedFileLengths = ref([]);
    return instance;
}