export const convertSecondsToTime = (sec) => {
    sec = Math.round(sec);
    let s = sec % 60;
    if(s < 10) s = '0'+s;
    let m = Math.floor(sec / 60) % 60;
    if(m < 10) m = '0'+m;
    const h = Math.floor(sec / 60 / 60);
    return h > 0 ? `${h}:${m}:${s}` : `${m}:${s}`;
}

export const getFeelGPTLabels = (segment, headersPositions, getTranslate) => {
    const feelPos = headersPositions['feelGPT'];
    if(!feelPos) return [];
    const value = segment[feelPos];

    return getFeelGPTLabelsByValue(value, getTranslate);
}

export const getFeelGPTLabelsByValue = (value, getTranslate = (t) => t ) => {
    return (value || "").split(';').map(i => i.replace(/^\[|]$/g, "")).filter(i => i)
        .map((i) => {
            const [text, value] = i.split(":");
            return {
                text: getTranslate(text) + ": " + value,
                className: text.replace(/^\s+|\s+$/g, "").replace(/\s/g, '-')
            };
        });
}
    
export const getValidSegments = (data, channel) => {
    const segments = data?.segments || data;

    const validPos = segments.headersPositions['validSegment'];
    const channelPos = segments.headersPositions['channel'];

    let validSegments = segments.data.filter(s => s[validPos]);

    const isSecondValid = isSecondChannelValid(segments);

    const parsedChannel = parseInt(channel);

    if(!Number.isFinite(parsedChannel)){
        if(!isSecondValid) validSegments = validSegments.filter(s => s[channelPos] === 0 );
    }
    else{
        if(parsedChannel === 1 && !isSecondValid){
            validSegments = [];
        }
        else validSegments = validSegments.filter(s => s[channelPos] === parsedChannel);
    }
    
    return validSegments;
}

export const isSecondChannelValid = (data) => {
    if(!data) return false;

    const reports = data.reports;
    if (reports && !reports['channel-1']) {
        return false;
    }
    
    const segments = data?.segments || data;
    
    const distributions = speakersDistribution(segments);
    
    const totalTalk = 100 - distributions.silence;
    const crossTalkPercentage = distributions.crosstalk / totalTalk * 100;
    
    if(distributions.channel1 === 0) return false;
    if(crossTalkPercentage > 50) return false;
    if(distributions.channel1 <= distributions.crosstalk) return false;

    return true;
}

export const speakersDistribution = (data) => {
    const segments = data?.segments || data;
    
    let result = {channel0: 0, silence: 0, crosstalk: 0, channel1: 0};

    if(!segments?.data) return result;

    const validSegments = segments.data.filter(s => s[headerPos(segments, 'validSegment')]);

    const channelPos = headerPos(segments, 'channel');
    const startPos = headerPos(segments, 'startPosSec');
    const endPos = headerPos(segments, 'endPosSec');

    let callLength = 0;
    let lastTimePos = 0;

    for(const segment of validSegments){
        if(segment[endPos] > callLength) callLength = segment[endPos];
        if(lastTimePos < segment[startPos]){
            result.silence += segment[startPos] - lastTimePos;
        }
        result[`channel${segment[channelPos]}`] += segment[endPos] - segment[startPos];

        lastTimePos = segment[endPos];
    }

    if(callLength > 0){
        if(result.channel0 + result.channel1 + result.silence > callLength){
            const cross = result.channel0 + result.channel1 + result.silence - callLength;
            result.crosstalk = cross;
            result.channel0 -= cross;
            result.channel1 -= cross;
        }
        result.channel0 = Math.round(result.channel0 / callLength * 100);
        result.channel1 = Math.round(result.channel1 / callLength * 100);
        result.silence = Math.round(result.silence / callLength * 100);
        result.crosstalk = Math.round(result.crosstalk / callLength * 100);
    }
    else result = {channel0: 0, silence: 0, crosstalk: 0, channel1: 0};

    return result;
}

export const headerPos = (data, header) => {
    const segments = data.segments || data;
    return segments.headersPositions[header];
}

export const getRiskType = (value) => {
    if(value >= 19) return "high";
    else if(value >= 18) return "medium";
    else if(value >= 17) return "suspected";
    return false;
}