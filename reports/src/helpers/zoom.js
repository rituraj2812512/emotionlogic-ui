﻿export function getZoom(ref){
    let parent = ref.value;
    let zoom = 1;

    do{
        zoom = zoom * parseFloat(getComputedStyle(parent).zoom || 1);
        parent = parent.parentElement || parent.getRootNode().host;
    } while(parent)

    return zoom;
}