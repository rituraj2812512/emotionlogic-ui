﻿export function invertColor(hexColor){
    const inRGB = hex2dec(hexColor);
    let newR = 255 - inRGB[1] - inRGB[2] + inRGB[0];
    let newG = 255 - inRGB[0] - inRGB[2] + inRGB[1];
    let newB = 255 - inRGB[0] - inRGB[1] + inRGB[2];
    if(newR < 0) newR = 0;
    if(newR > 255) newR = 255;
    if(newG < 0) newG = 0;
    if(newG > 255) newG = 255;
    if(newB < 0) newB = 0;
    if(newB > 255) newB = 255;
    return rgb2hex(newR, newG, newB);
}

export function mixColor(base, need, intensity){
    if(intensity < 0) intensity = 0;
    if(intensity > 255) intensity = 255;
    const baseRGB = hex2dec(base);
    const needRGB = hex2dec(need);
    const iPercent = intensity / 255;
    const r = baseRGB[0]*(1 - iPercent) + needRGB[0]*(iPercent);
    const g = baseRGB[1]*(1 - iPercent) + needRGB[1]*(iPercent);
    const b = baseRGB[2]*(1 - iPercent) + needRGB[2]*(iPercent);
    return rgb2hex(r, g, b);
}

function hex2dec(hex) {
    return hex.replace('#', '').match(/.{2}/g).map(n => parseInt(n, 16));
}
function rgb2hex(r, g, b) {
    r = Math.round(r);
    g = Math.round(g);
    b = Math.round(b);
    r = Math.min(r, 255);
    g = Math.min(g, 255);
    b = Math.min(b, 255);
    return '#' + [r, g, b].map(c => c.toString(16).padStart(2, '0')).join('');
}
