import * as Components from './_exportComponents.js'

export function registerQuestionnaireTopicHeader(htmlTag = 'nms-dr-questionnaire-topic-header') {
  customElements.define(htmlTag, Components.QuestionnaireTopicHeader)
}
export function registerQuestionnaireTopic(htmlTag = 'nms-dr-questionnaire-topic') {
  customElements.define(htmlTag, Components.QuestionnaireTopic)
}
export function registerQuestionnaireQuestionHeader(htmlTag = 'nms-dr-questionnaire-question-header') {
  customElements.define(htmlTag, Components.QuestionnaireQuestionHeader)
}
export function registerQuestionnaireQuestion(htmlTag = 'nms-dr-questionnaire-question') {
  customElements.define(htmlTag, Components.QuestionnaireQuestion)
}
export function registerQuestionnaireQuestionObjectiveRisk(htmlTag = 'nms-dr-questionnaire-question-objective-risk') {
  customElements.define(htmlTag, Components.QuestionnaireQuestionObjectiveRisk)
}
export function registerQuestionnaireQuestionSubjectiveRisk(htmlTag = 'nms-dr-questionnaire-question-subjective-risk') {
  customElements.define(htmlTag, Components.QuestionnaireQuestionSubjectiveRisk)
}
export function registerQuestionnaireQuestionRiskSummary(htmlTag = 'nms-dr-questionnaire-question-risk-summary') {
  customElements.define(htmlTag, Components.QuestionnaireQuestionRiskSummary)
}
export function registerQuestionnaireQuestionTranscription(htmlTag = 'nms-dr-questionnaire-question-transcription') {
  customElements.define(htmlTag, Components.QuestionnaireQuestionTranscription)
}
export function registerQuestionnaireTopicList(htmlTag = 'nms-dr-questionnaire-topic-list') {
  customElements.define(htmlTag, Components.QuestionnaireTopicList)
}