import { defineCustomElement } from 'vue'

import _TopicHeader from './TopicHeader.ce.vue'
import _Topic from './Topic.ce.vue'
import _QuestionHeader from './QuestionHeader.ce.vue'
import _Question from './Question.ce.vue'
import _QuestionObjectiveRisk from './QuestionObjectiveRisk.ce.vue'
import _QuestionSubjectiveRisk from './QuestionSubjectiveRisk.ce.vue'
import _QuestionRiskSummary from './QuestionRiskSummary.ce.vue'
import _QuestionTranscription from './QuestionTranscription.ce.vue'
import _TopicList from './TopicList.ce.vue'

export const QuestionnaireTopicHeader = defineCustomElement(_TopicHeader)
export const QuestionnaireTopic = defineCustomElement(_Topic)
export const QuestionnaireQuestionHeader = defineCustomElement(_QuestionHeader)
export const QuestionnaireQuestion = defineCustomElement(_Question)
export const QuestionnaireQuestionObjectiveRisk = defineCustomElement(_QuestionObjectiveRisk)
export const QuestionnaireQuestionSubjectiveRisk = defineCustomElement(_QuestionSubjectiveRisk)
export const QuestionnaireQuestionRiskSummary = defineCustomElement(_QuestionRiskSummary)
export const QuestionnaireQuestionTranscription = defineCustomElement(_QuestionTranscription)
export const QuestionnaireTopicList = defineCustomElement(_TopicList)
