import * as Components from './_exportComponents.js'

export function registerConclusion(htmlTag = 'nms-dr-conclusion') {
  customElements.define(htmlTag, Components.Conclusion)
}