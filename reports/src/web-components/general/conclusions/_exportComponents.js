import { defineCustomElement } from 'vue'

import _Conclusion from './Conclusion.ce.vue'

export const Conclusion = defineCustomElement(_Conclusion);