﻿function useClassificationFeature(conclusion) {

    const title = (() => {
        const text = String(conclusion || "");
        const m = text.match(/#[a-z\-]+/i) || [];
        conclusion = conclusion.replace(m[0], '');
        return (m[0] || '').replace('#', '').replace('-', ' ').toLowerCase();
    })()

    const titleColor = (() => {
        const text = String(conclusion || "");
        const m = text.match(/@(yellow|red|green|blue|purple)/i) || [];
        conclusion = conclusion.replace(m[0], '');
        return m[1] ? {color: m[1].toLowerCase() === "yellow" ? "orange" : m[1]} : {};
    })()

    const items = (() => {
        const text = conclusion || '';
        // const sections = [
        //     { search: '<Full Description>', header: t("feelGPT.result.fullDescription")},
        //     { search: '<Conclusions>', header: t("feelGPT.result.conclusions")},
        //     { search: '<Call To Action>', header: t("feelGPT.result.callToAction")},
        // ];
        // const matches = sections.map(i => [i, text.indexOf(i.search)]).sort((x,y) => x[1] - y[1]).filter(i => i[1] > -1);
        //
        // if (!matches.length) {
        //     return [{
        //         text,
        //     }]
        // }
        //
        // const res = [];
        //
        // const pre = matches[0][1] > 0 ? text.substring(0, matches[0][1]) : '';
        // if (pre) {
        //     res.push({text: pre});
        // }
        //
        // for (let i = 0; i < matches.length; i++) {
        //     const match = matches[i];
        //     const nextMatch = matches[i + 1];
        //     const matchText = nextMatch ? text.substring(match[1] + match[0].search.length, nextMatch[1]) : text.substring(match[1] + match[0].search.length);
        //     res.push({header: match[0].header, text: matchText});
        // }
        //
        // return res;

        const p = /<([^>]+)>\s*/g;
        let match;
        const matches = [];
        while ((match = p.exec(text)) !== null) {
            matches.push(match);
        }

        if (!matches.length) {
            return [{
                text,
            }]
        }

        const res = [];

        const pre = matches[0].index > 0 ? text.substring(0, matches[0].index) : '';
        if (pre) {
            res.push({text: pre});
        }

        for (let i = 0; i < matches.length; i++) {
            const match = matches[i];
            const nextMatch = matches[i + 1];
            const matchText = nextMatch ?
                text.substring(match.index + match[0].length, nextMatch.index) :
                text.substring(match.index + match[0].length);
            res.push({header: match[1], text: matchText});
        }

        return res;
    })()

    return {
        title,
        titleColor,
        items,
    };
}

export {
    useClassificationFeature,
}

