import * as Components from './_exportComponents.js'

export function registerGeneralRiskScore(htmlTag = 'nms-dr-general-risk-score') {
  customElements.define(htmlTag, Components.GeneralRiskScore)
}
export function registerRiskLineTopic(htmlTag = 'nms-dr-risk-line-topic') {
  customElements.define(htmlTag, Components.RiskLineTopic)
}
export function registerRiskBriefTopic(htmlTag = 'nms-dr-risk-brief-topic') {
  customElements.define(htmlTag, Components.RiskBriefTopic)
}
export function registerRiskBriefTopicList(htmlTag = 'nms-dr-risk-brief-topic-list') {
  customElements.define(htmlTag, Components.RiskBriefTopicList)
}
export function registerRiskBriefTopicListCollapse(htmlTag = 'nms-dr-risk-brief-topic-list-collapse') {
  customElements.define(htmlTag, Components.RiskBriefTopicListCollapse)
}
export function registerRiskBriefHighlightGeneral(htmlTag = 'nms-dr-risk-brief-highlight-general') {
  customElements.define(htmlTag, Components.RiskBriefHighlightGeneral)
}
export function registerRiskBriefHighlightDetailed(htmlTag = 'nms-dr-risk-brief-highlight-detailed') {
  customElements.define(htmlTag, Components.RiskBriefHighlightDetailed)
}
export function registerRiskBriefHighlight(htmlTag = 'nms-dr-risk-brief-highlight') {
  customElements.define(htmlTag, Components.RiskBriefHighlight)
}
export function registerRiskBriefHighlightCollapse(htmlTag = 'nms-dr-risk-brief-highlight-collapse') {
  customElements.define(htmlTag, Components.RiskBriefHighlightCollapse)
}
export function registerSTTSegmentAnalysisList(htmlTag = 'nms-dr-stt-segment-analysis-list') {
  customElements.define(htmlTag, Components.STTSegmentAnalysisList)
}
export function registerRiskBadgeIndicators(htmlTag = 'nms-dr-rbi-list') {
  customElements.define(htmlTag, Components.RiskBadgeIndicators)
}