import { defineCustomElement } from 'vue'

import _GeneralRiskScore from './GeneralRiskScore.ce.vue'
import _LineTopic from './LineTopic.ce.vue'
import _BriefTopic from './BriefTopic.ce.vue'
import _BriefTopicList from './BriefTopicList.ce.vue'
import _BriefTopicListCollapse from './BriefTopicListCollapse.ce.vue'
import _BriefHighlightGeneral from './BriefHighlightGeneral.ce.vue'
import _BriefHighlightDetailed from './BriefHighlightDetailed.ce.vue'
import _BriefHighlight from './BriefHighlight.ce.vue'
import _BriefHighlightCollapse from './BriefHighlightCollapse.ce.vue'
import _STTSegmentAnalysisList from './STTSegmentAnalysisList.ce.vue'
import _RiskBadgeIndicators from './RiskBadgeIndicators.ce.vue'

export const GeneralRiskScore = defineCustomElement(_GeneralRiskScore)
export const RiskLineTopic = defineCustomElement(_LineTopic)
export const RiskBriefTopic = defineCustomElement(_BriefTopic)
export const RiskBriefTopicList = defineCustomElement(_BriefTopicList)
export const RiskBriefTopicListCollapse = defineCustomElement(_BriefTopicListCollapse)
export const RiskBriefHighlightGeneral = defineCustomElement(_BriefHighlightGeneral)
export const RiskBriefHighlightDetailed = defineCustomElement(_BriefHighlightDetailed)
export const RiskBriefHighlight = defineCustomElement(_BriefHighlight)
export const RiskBriefHighlightCollapse = defineCustomElement(_BriefHighlightCollapse)
export const STTSegmentAnalysisList = defineCustomElement(_STTSegmentAnalysisList)
export const RiskBadgeIndicators = defineCustomElement(_RiskBadgeIndicators)
