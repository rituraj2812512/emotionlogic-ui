import { defineCustomElement } from 'vue'

import _Title from './Title.ce.vue'

export const Title = defineCustomElement(_Title);