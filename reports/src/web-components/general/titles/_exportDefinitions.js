import * as Components from './_exportComponents.js'

export function registerTitle(htmlTag = 'nms-dr-title') {
  customElements.define(htmlTag, Components.Title)
}