import { defineCustomElement } from 'vue'

import _EmotionPlayer from './EmotionPlayer.ce.vue'

export const EmotionPlayer = defineCustomElement(_EmotionPlayer)