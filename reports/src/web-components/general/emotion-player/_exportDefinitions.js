import * as Components from './_exportComponents.js'

export function registerEmotionPlayer(htmlTag = 'nms-dr-emotion-player') {
  customElements.define(htmlTag, Components.EmotionPlayer)
}