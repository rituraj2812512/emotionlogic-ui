﻿import {ref, watch, computed} from "vue";
import {
    getEnergyLine,
    getEmotionsLine,
    getRiskLine,
    getStressLine,
    getSpeakersLine
} from '@/components/player/helpers/defaultRenderLineOptions.js'
import {isSecondChannelValid} from "@/helpers/segments.js";

const defaultLineOptions = {
    baseColor: "#000000", //baseColor: "#DFE3EF",
    invertColors: false, //for light baseColor - true;   for black - false
    riskOnProgressLine: false,
}

export function getRenderOptions(props, Locale){
    const totalAudioLength = ref(0);
    const lines = ref([]);
    const isMultichannel = ref(false);
    const _restCalcFileLength = ref(0);

    /*function validateThisChannel(channel){ //Example
        return (segment) => {
            const valueId = props.value.options.segments.headersPositions['channel'];
            return segment[valueId] === channel
        }
    }*/

    //multichannel
    watch([() => props.value.options?.segments], () => {
        isMultichannel.value = isSecondChannelValid(props.value.options.segments);
    }, {immediate: true})
    
    //totalAudioLength
    watch(() => props.value?.options?.files, () => {
        let files = props.value?.options?.files;
        
        if(!files || !Array.isArray(files) || files.some(v => !v)) return;
        
        if(typeof files[0] === 'object') files = files.map(f => f.url);
        
        _restCalcFileLength.value += props.value?.options?.files.length;
        let _totalAudioLength = 0;

        files.forEach((file) => {
            const audio = new Audio();
            audio.addEventListener("loadedmetadata", function(){
                _totalAudioLength += audio.duration;
                _restCalcFileLength.value -= 1;
                if(_restCalcFileLength.value === 0) totalAudioLength.value = _totalAudioLength;
            });
            audio.crossOrigin = "anonymous";
            audio.src = file;
        })
    }, {immediate: true});

    //lines
    watch([() => props.value.options?.segments, () => props.value.options?.lines], () => {
        const segments = props.value.options?.segments;
        const optionLines = props.value.options?.lines ? {...defaultLineOptions, ...props.value.options?.lines} : defaultLineOptions;
        let lineSettings = [];
        
    
        if(!optionLines.riskOnProgressLine){
            if(segments.headersPositions.hasOwnProperty("EmotionPlayer-Risk")) lineSettings.push(getRiskLine(Locale, isMultichannel, optionLines))    
        }
        lineSettings.push(getEmotionsLine(Locale, isMultichannel, optionLines))
        lineSettings.push(getStressLine(Locale, isMultichannel, optionLines))
        lineSettings.push(getEnergyLine(Locale, isMultichannel, optionLines))
        if(isMultichannel.value){
            lineSettings.push(getSpeakersLine(Locale, isMultichannel, optionLines))
        }
        
        //if(segments.headersPositions.hasOwnProperty("question"))
        
        lines.value = lineSettings;
    }, {immediate: true})
    
    return computed(() => {
        return {
            lines: lines.value,
            totalAudioLength: totalAudioLength.value,
        }
    })
}  