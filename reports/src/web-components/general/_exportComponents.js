import { defineCustomElement } from 'vue'

import _Player from '@/components/player/Player.ce.vue'

export * from './badge/_exportComponents.js'
export * from './emotion-player/_exportComponents.js'
export * from './summary/_exportComponents.js'
export * from './risk/_exportComponents.js'
export * from './questionnaire/_exportComponents.js'
export * from './interview/_exportComponents.js'
export * from './titles/_exportComponents.js'
export * from './conclusions/_exportComponents.js'

export const AudioPlayer = defineCustomElement(_Player)