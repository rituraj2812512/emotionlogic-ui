import { defineCustomElement } from 'vue'

import _BadgeAnticipation from './BadgeAnticipation.ce.vue'
import _BadgeChurnRisk from './BadgeChurnRisk.ce.vue'
import _BadgeFinalScore from './BadgeFinalScore.ce.vue'
import _BadgeTrainingNeeds from './BadgeTrainingNeeds.ce.vue'

export const BadgeAnticipation = defineCustomElement(_BadgeAnticipation)
export const BadgeChurnRisk = defineCustomElement(_BadgeChurnRisk)
export const BadgeFinalScore = defineCustomElement(_BadgeFinalScore)
export const BadgeTrainingNeeds = defineCustomElement(_BadgeTrainingNeeds)