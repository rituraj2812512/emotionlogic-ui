import * as Components from './_exportComponents.js'

export function registerBadgeAnticipation(htmlTag = 'nms-dr-badge-anticipation') {
  customElements.define(htmlTag, Components.BadgeAnticipation)
}
export function registerBadgeChurnRisk(htmlTag = 'nms-dr-badge-churn-risk') {
  customElements.define(htmlTag, Components.BadgeChurnRisk)
}
export function registerBadgeFinalScore(htmlTag = 'nms-dr-badge-final-score') {
  customElements.define(htmlTag, Components.BadgeFinalScore)
}
export function registerBadgeTrainingNeeds(htmlTag = 'nms-dr-badge-training-needs') {
  customElements.define(htmlTag, Components.BadgeTrainingNeeds)
}