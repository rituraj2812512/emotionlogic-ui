import { defineCustomElement } from 'vue'

import _TopicHeader from './TopicHeader.ce.vue'
import _StrengthWeaknessListCollapse from './StrengthWeaknessListCollapse.ce.vue'
import _StrengthWeaknessList from './StrengthWeaknessList.ce.vue'
import _StrengthWeakness from './StrengthWeakness.ce.vue'
import _Question from './Question.ce.vue'
import _LogicEmotionRatio from './LogicEmotionRatio.ce.vue'
import _GenuineSocialRatio from './GenuineSocialRatio.ce.vue'
import _StrengthWeaknessAnalysis from './StrengthWeaknessAnalysis.ce.vue'
import _KeyEmotions from './KeyEmotions.ce.vue'
import _Topic from './Topic.ce.vue'
import _TopicList from './TopicList.ce.vue'
import _GenuineDesire from './GenuineDesire.ce.vue'
import _CarefulFreelyRatio from './CarefulFreelyRatio.ce.vue'

export const InterviewTopicHeader = defineCustomElement(_TopicHeader)
export const InterviewStrengthWeaknessListCollapse = defineCustomElement(_StrengthWeaknessListCollapse)
export const InterviewStrengthWeaknessList = defineCustomElement(_StrengthWeaknessList)
export const InterviewStrengthWeakness = defineCustomElement(_StrengthWeakness)
export const InterviewQuestion = defineCustomElement(_Question)
export const InterviewLogicEmotionRatio = defineCustomElement(_LogicEmotionRatio)
export const InterviewGenuineSocialRatio = defineCustomElement(_GenuineSocialRatio)
export const InterviewStrengthWeaknessAnalysis = defineCustomElement(_StrengthWeaknessAnalysis)
export const InterviewKeyEmotions = defineCustomElement(_KeyEmotions)
export const InterviewTopic = defineCustomElement(_Topic)
export const InterviewTopicList = defineCustomElement(_TopicList)
export const InterviewGenuineDesire = defineCustomElement(_GenuineDesire)
export const InterviewCarefulFreelyRatio = defineCustomElement(_CarefulFreelyRatio)
