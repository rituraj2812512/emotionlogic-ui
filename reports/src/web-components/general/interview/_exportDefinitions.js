import * as Components from './_exportComponents.js'

export function registerInterviewTopicHeader(htmlTag = 'nms-dr-interview-topic-header') {
  customElements.define(htmlTag, Components.InterviewTopicHeader)
}
export function registerInterviewStrengthWeaknessListCollapse(htmlTag = 'nms-dr-interview-sw-collapse') {
  customElements.define(htmlTag, Components.InterviewStrengthWeaknessListCollapse)
}
export function registerInterviewStrengthWeaknessList(htmlTag = 'nms-dr-interview-sw-list') {
  customElements.define(htmlTag, Components.InterviewStrengthWeaknessList)
}
export function registerInterviewStrengthWeakness(htmlTag = 'nms-dr-interview-sw') {
  customElements.define(htmlTag, Components.InterviewStrengthWeakness)
}
export function registerInterviewQuestion(htmlTag = 'nms-dr-interview-question') {
  customElements.define(htmlTag, Components.InterviewQuestion)
}
export function registerInterviewLogicEmotionRatio(htmlTag = 'nms-dr-interview-logic-emotion-ratio') {
  customElements.define(htmlTag, Components.InterviewLogicEmotionRatio)
}
export function registerInterviewGenuineSocialRatio(htmlTag = 'nms-dr-interview-genuine-social-ratio') {
  customElements.define(htmlTag, Components.InterviewGenuineSocialRatio)
}
export function registerInterviewStrengthWeaknessAnalysis(htmlTag = 'nms-dr-interview-sw-analysis') {
  customElements.define(htmlTag, Components.InterviewStrengthWeaknessAnalysis)
}
export function registerInterviewKeyEmotions(htmlTag = 'nms-dr-interview-key-emotions') {
  customElements.define(htmlTag, Components.InterviewKeyEmotions)
}
export function registerInterviewTopic(htmlTag = 'nms-dr-interview-topic') {
  customElements.define(htmlTag, Components.InterviewTopic)
}
export function registerInterviewTopicList(htmlTag = 'nms-dr-interview-topic-list') {
  customElements.define(htmlTag, Components.InterviewTopicList)
}
export function registerInterviewGenuineDesire(htmlTag = 'nms-dr-interview-genuine-desire') {
  customElements.define(htmlTag, Components.InterviewGenuineDesire)
}
export function registerInterviewCarefulFreelyRatio(htmlTag = 'nms-dr-interview-careful-freely-ratio') { 
  customElements.define(htmlTag, Components.InterviewCarefulFreelyRatio)
}