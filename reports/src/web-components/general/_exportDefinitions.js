import * as Components from './_exportComponents.js'

export * from './badge/_exportDefinitions.js'
export * from './emotion-player/_exportDefinitions.js'
export * from './summary/_exportDefinitions.js'
export * from './risk/_exportDefinitions.js'
export * from './questionnaire/_exportDefinitions.js'
export * from './interview/_exportDefinitions.js'
export * from './titles/_exportDefinitions.js'
export * from './conclusions/_exportDefinitions.js'

export function registerAudioPlayer(htmlTag = 'nms-dr-audio-player') {
    customElements.define(htmlTag, Components.AudioPlayer)
}