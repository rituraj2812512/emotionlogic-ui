import { defineCustomElement } from 'vue'

import _SpeakerDistribution from './SpeakerDistribution.ce.vue'
import _GeneralSummaryHeader from './GeneralSummaryHeader.ce.vue'
import _TechnicalInfo from './TechnicalInfo.ce.vue'
import _GeneralSummaryCallType from './GeneralSummaryCallType.ce.vue'
import _GeneralSummaryPriority from './GeneralSummaryPriority.ce.vue'
import _GeneralSummaryRecommendation from './GeneralSummaryRecommendation.ce.vue'
import _GeneralSummarySection from './GeneralSummarySection.ce.vue'
import _GeneralSummary from './GeneralSummary.ce.vue'
import _TimeStampEmotions from './TimeStampEmotions.ce.vue'

export const SpeakerDistribution = defineCustomElement(_SpeakerDistribution)
export const GeneralSummaryHeader = defineCustomElement(_GeneralSummaryHeader)
export const TechnicalInfo = defineCustomElement(_TechnicalInfo)
export const GeneralSummaryCallType = defineCustomElement(_GeneralSummaryCallType)
export const GeneralSummaryPriority = defineCustomElement(_GeneralSummaryPriority)
export const GeneralSummaryRecommendation = defineCustomElement(_GeneralSummaryRecommendation)
export const GeneralSummarySection = defineCustomElement(_GeneralSummarySection)
export const GeneralSummary = defineCustomElement(_GeneralSummary)
export const TimeStampEmotions = defineCustomElement(_TimeStampEmotions)
