import * as Components from './_exportComponents.js'

export function registerSpeakerDistribution(htmlTag = 'nms-dr-speaker-distribution') {
  customElements.define(htmlTag, Components.SpeakerDistribution)
}
export function registerGeneralSummaryHeader(htmlTag = 'nms-dr-general-summary-header') {
  customElements.define(htmlTag, Components.GeneralSummaryHeader)
}
export function registerTechnicalInfo(htmlTag = 'nms-dr-technical-info') {
  customElements.define(htmlTag, Components.TechnicalInfo)
}
export function registerGeneralSummaryCallType(htmlTag = 'nms-dr-general-summary-call-type') {
  customElements.define(htmlTag, Components.GeneralSummaryCallType)
}
export function registerGeneralSummaryPriority(htmlTag = 'nms-dr-general-summary-priority') {
  customElements.define(htmlTag, Components.GeneralSummaryPriority)
}
export function registerGeneralSummaryRecommendation(htmlTag = 'nms-dr-general-summary-recommendation') {
  customElements.define(htmlTag, Components.GeneralSummaryRecommendation)
}
export function registerGeneralSummarySection(htmlTag = 'nms-dr-general-summary-section') {
  customElements.define(htmlTag, Components.GeneralSummarySection)
}
export function registerGeneralSummary(htmlTag = 'nms-dr-general-summary') {
  customElements.define(htmlTag, Components.GeneralSummary)
}
export function registerTimeStampEmotions(htmlTag = 'nms-dr-timestamp-emotions') {
  customElements.define(htmlTag, Components.TimeStampEmotions)
}