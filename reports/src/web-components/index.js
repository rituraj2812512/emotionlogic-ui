import Locale from '@/locales/locale.json'
import * as GeneralDefinitions from '@/web-components/general/_exportDefinitions.js'
import * as ChannelDefinitions from '@/web-components/channel/_exportDefinitions.js'

export * from '@/web-components/general/_exportComponents.js'
export * from '@/web-components/general/_exportDefinitions.js'

export * from '@/web-components/channel/_exportComponents.js'
export * from '@/web-components/channel/_exportDefinitions.js'

const allDefinitions = {
  ...GeneralDefinitions,
  ...ChannelDefinitions,
}

export {
  Locale,
  registerAllComponents,
  setProps
}

window.NMSDR = {
  Locale,
  setProps,
  registerAllComponents,
  ...allDefinitions,
}

function registerAllComponents() {
  for(const key in allDefinitions){
    try{
      allDefinitions[key]();
    }
    catch (e) {}
  }
}

function setProps(selector, props) {
  const element = document.querySelector(selector);
  if(!element) return 'element not found';
  if(typeof props !== "object") return 'props must be an object';
  for(let key in props){
    if(props[key] === undefined) continue;
    let propName = camelCaseToKebabCase(key);
    let propValue = props[key];
    if(typeof propValue !== "string") propValue = JSON.stringify(propValue);

    element.setAttribute(propName, propValue)
  }
}

function camelCaseToKebabCase(str) {
  return str.replace(/[A-Z][a-z]*/g, str => '-' + str.toLowerCase())
      .replace('--', '-') // remove double hyphens
      .replace(/(^-)|(-$)/g, ''); // remove hyphens at the beginning and the end
}