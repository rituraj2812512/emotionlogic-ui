import { defineCustomElement } from 'vue'

import _ChannelClassification from './ChannelClassification.ce.vue'
import _ChannelSatisfactionScore from './ChannelSatisfactionScore.ce.vue'
import _ChannelPriorityReductionScore from './ChannelPriorityReductionScore.ce.vue'
import _ChannelSegmentsVolume from './ChannelSegmentsVolume.ce.vue'
import _ChannelFinal10SegmentsAnalysis from './ChannelFinal10SegmentsAnalysis.ce.vue'
import _ChannelSummary from './ChannelSummary.ce.vue'
import _ChannelEDP from './ChannelEDP.ce.vue'
import _ChannelDistressBadge from './ChannelDistressBadge.ce.vue'
import _ChannelEngagementBadge from './ChannelEngagementBadge.ce.vue'
import _ChannelHesitationBadge from './ChannelHesitationBadge.ce.vue'

export const ChannelSummaryClassification = defineCustomElement(_ChannelClassification)
export const ChannelSummarySatisfactionScore = defineCustomElement(_ChannelSatisfactionScore)
export const ChannelSummaryPriorityReductionScore = defineCustomElement(_ChannelPriorityReductionScore)
export const ChannelSummarySegmentsVolume = defineCustomElement(_ChannelSegmentsVolume)
export const ChannelSummaryFinal10SegmentsAnalysis = defineCustomElement(_ChannelFinal10SegmentsAnalysis)
export const ChannelSummary = defineCustomElement(_ChannelSummary)
export const ChannelEDP = defineCustomElement(_ChannelEDP)
export const ChannelDistressBadge = defineCustomElement(_ChannelDistressBadge)
export const ChannelEngagementBadge = defineCustomElement(_ChannelEngagementBadge)
export const ChannelHesitationBadge = defineCustomElement(_ChannelHesitationBadge)