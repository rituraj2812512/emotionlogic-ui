import * as Components from './_exportComponents.js'

export function registerChannelSummaryClassification(htmlTag = 'nms-dr-channel-summary-classification') {
  customElements.define(htmlTag, Components.ChannelSummaryClassification)
}
export function registerChannelSummarySatisfactionScore(htmlTag = 'nms-dr-channel-summary-satisfaction-score') {
  customElements.define(htmlTag, Components.ChannelSummarySatisfactionScore)
}
export function registerChannelSummaryPriorityReductionScore(htmlTag = 'nms-dr-channel-summary-priority-reduction-score') {
  customElements.define(htmlTag, Components.ChannelSummaryPriorityReductionScore)
}
export function registerChannelSummarySegmentsVolume(htmlTag = 'nms-dr-channel-summary-segments-volume') {
  customElements.define(htmlTag, Components.ChannelSummarySegmentsVolume)
}
export function registerChannelSummaryFinal10SegmentsAnalysis(htmlTag = 'nms-dr-channel-summary-final-segments-analysis') {
  customElements.define(htmlTag, Components.ChannelSummaryFinal10SegmentsAnalysis)
}
export function registerChannelSummary(htmlTag = 'nms-dr-channel-summary') {
  customElements.define(htmlTag, Components.ChannelSummary)
}
export function registerChannelEDP(htmlTag = 'nms-dr-channel-edp') {
  customElements.define(htmlTag, Components.ChannelEDP)
}
export function registerChannelDistressBadge(htmlTag = 'nms-dr-channel-db') {
  customElements.define(htmlTag, Components.ChannelDistressBadge)
}
export function registerChannelEngagementBadge(htmlTag = 'nms-dr-channel-eb') {
  customElements.define(htmlTag, Components.ChannelEngagementBadge)
}
export function registerChannelHesitationBadge(htmlTag = 'nms-dr-channel-hb') {
  customElements.define(htmlTag, Components.ChannelHesitationBadge)
}