import { defineCustomElement } from 'vue'

import _ChannelAVGStressScore from './ChannelAVGStressScore.ce.vue'
import _ChannelCLStressScore from './ChannelCLStressScore.ce.vue'
import _StressProfileCLStress from './StressProfileCLStress.ce.vue'
import _StressProfileStress from './StressProfileStress.ce.vue'
import _StressProfileExtreme from './StressProfileExtreme.ce.vue'
import _StressProfileHeader from './StressProfileHeader.ce.vue'
import _StressProfile from './StressProfile.ce.vue'

export const StressProfileAVGStressScore = defineCustomElement(_ChannelAVGStressScore)
export const StressProfileCLStressScore = defineCustomElement(_ChannelCLStressScore)
export const StressProfileCLStress = defineCustomElement(_StressProfileCLStress)
export const StressProfileStress = defineCustomElement(_StressProfileStress)
export const StressProfileExtreme = defineCustomElement(_StressProfileExtreme)
export const StressProfileHeader = defineCustomElement(_StressProfileHeader)
export const StressProfile = defineCustomElement(_StressProfile)