import * as Components from './_exportComponents.js'

export function registerStressProfileAVGStressScore(htmlTag = 'nms-dr-stress-profile-avg-stress-score') {
  customElements.define(htmlTag, Components.StressProfileAVGStressScore)
}
export function registerStressProfileCLStressScore(htmlTag = 'nms-dr-stress-profile-cl-stress-score') {
  customElements.define(htmlTag, Components.StressProfileCLStressScore)
}
export function registerStressProfileCLStress(htmlTag = 'nms-dr-stress-profile-cl-stress') {
  customElements.define(htmlTag, Components.StressProfileCLStress)
}
export function registerStressProfileStress(htmlTag = 'nms-dr-stress-profile-stress') {
  customElements.define(htmlTag, Components.StressProfileStress)
}
export function registerStressProfileExtreme(htmlTag = 'nms-dr-stress-profile-extreme') {
  customElements.define(htmlTag, Components.StressProfileExtreme)
}
export function registerStressProfileHeader(htmlTag = 'nms-dr-stress-profile-header') {
  customElements.define(htmlTag, Components.StressProfileHeader)
}
export function registerStressProfile(htmlTag = 'nms-dr-stress-profile') {
  customElements.define(htmlTag, Components.StressProfile)
}