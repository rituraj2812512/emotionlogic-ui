import * as Components from './_exportComponents.js'

export function registerLogicalProfileImagination(htmlTag = 'nms-dr-logical-profile-imagination') {
  customElements.define(htmlTag, Components.LogicalProfileImagination)
}
export function registerLogicalProfileMentalEffort(htmlTag = 'nms-dr-logical-profile-mental-effort') {
  customElements.define(htmlTag, Components.LogicalProfileMentalEffort)
}
export function registerLogicalProfileMentalEffortEfficiency(htmlTag = 'nms-dr-logical-profile-mental-effort-efficiency') {
  customElements.define(htmlTag, Components.LogicalProfileMentalEffortEfficiency)
}
export function registerLogicalProfileUncertainty(htmlTag = 'nms-dr-logical-profile-uncertainty') {
  customElements.define(htmlTag, Components.LogicalProfileUncertainty)
}
export function registerLogicalProfile(htmlTag = 'nms-dr-logical-profile') {
  customElements.define(htmlTag, Components.LogicalProfile)
}
export function registerLogicalProfileHeader(htmlTag = 'nms-dr-logical-profile-header') {
  customElements.define(htmlTag, Components.LogicalProfileHeader)
}