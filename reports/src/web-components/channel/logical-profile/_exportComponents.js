import { defineCustomElement } from 'vue'

import _ChartImagination from './ChartImagination.ce.vue'
import _ChartMentalEffort from './ChartMentalEffort.ce.vue'
import _ChartMentalEffortEfficiency from './ChartMentalEffortEfficiency.ce.vue'
import _ChartUncertainty from './ChartUncertainty.ce.vue'
import _LogicalProfile from './LogicalProfile.ce.vue'
import _LogicalProfileHeader from './LogicalProfileHeader.ce.vue'

export const LogicalProfileImagination = defineCustomElement(_ChartImagination)
export const LogicalProfileMentalEffort = defineCustomElement(_ChartMentalEffort)
export const LogicalProfileMentalEffortEfficiency = defineCustomElement(_ChartMentalEffortEfficiency)
export const LogicalProfileUncertainty = defineCustomElement(_ChartUncertainty)
export const LogicalProfile = defineCustomElement(_LogicalProfile)
export const LogicalProfileHeader = defineCustomElement(_LogicalProfileHeader)