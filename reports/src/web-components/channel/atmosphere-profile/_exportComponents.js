import { defineCustomElement } from 'vue'

import _AtmosphereProfile from './AtmosphereProfile.ce.vue'
import _ChartAtmosphere from './ChartAtmosphere.ce.vue'
import _ChartDiscomfort from './ChartDiscomfort.ce.vue'

export const AtmosphereProfile = defineCustomElement(_AtmosphereProfile)
export const AtmosphereProfileAtmosphere = defineCustomElement(_ChartAtmosphere)
export const AtmosphereProfileDiscomfort = defineCustomElement(_ChartDiscomfort)
