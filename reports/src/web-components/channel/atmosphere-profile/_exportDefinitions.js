import * as Components from './_exportComponents.js'

export function registerAtmosphereProfile(htmlTag = 'nms-dr-atmosphere-profile') {
  customElements.define(htmlTag, Components.AtmosphereProfile)
}
export function registerAtmosphereProfileAtmosphere(htmlTag = 'nms-dr-atmosphere-profile-atmosphere') {
  customElements.define(htmlTag, Components.AtmosphereProfileAtmosphere)
}
export function registerAtmosphereProfileDiscomfort(htmlTag = 'nms-dr-atmosphere-profile-discomfort') {
  customElements.define(htmlTag, Components.AtmosphereProfileDiscomfort)
}