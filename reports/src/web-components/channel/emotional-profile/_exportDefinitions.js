import * as Components from './_exportComponents.js'

export function registerEmotionalProfile(htmlTag = 'nms-dr-emotional-profile') {
  customElements.define(htmlTag, Components.EmotionalProfile)
}
export function registerEmotionalProfileHeader(htmlTag = 'nms-dr-emotional-profile-header') {
  customElements.define(htmlTag, Components.EmotionalProfileHeader)
}
export function registerEmotionalProfileArousal(htmlTag = 'nms-dr-emotional-profile-arousal') {
  customElements.define(htmlTag, Components.EmotionalProfileArousal)
}
export function registerEmotionalProfileExcitement(htmlTag = 'nms-dr-emotional-profile-excitement') {
  customElements.define(htmlTag, Components.EmotionalProfileExcitement)
}
export function registerEmotionalProfileUneasiness(htmlTag = 'nms-dr-emotional-profile-uneasiness') {
  customElements.define(htmlTag, Components.EmotionalProfileUneasiness)
}