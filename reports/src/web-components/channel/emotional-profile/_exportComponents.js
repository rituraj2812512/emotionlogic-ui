import { defineCustomElement } from 'vue'

import _EmotionalProfile from './EmotionalProfile.ce.vue'
import _EmotionalProfileHeader from './EmotionalProfileHeader.ce.vue'
import _RatioArousal from './RatioArousal.ce.vue'
import _RatioExcitement from './RatioExcitement.ce.vue'
import _RatioUneasiness from './RatioUneasiness.ce.vue'

export const EmotionalProfile = defineCustomElement(_EmotionalProfile)
export const EmotionalProfileHeader = defineCustomElement(_EmotionalProfileHeader)
export const EmotionalProfileArousal = defineCustomElement(_RatioArousal)
export const EmotionalProfileExcitement = defineCustomElement(_RatioExcitement)
export const EmotionalProfileUneasiness = defineCustomElement(_RatioUneasiness)