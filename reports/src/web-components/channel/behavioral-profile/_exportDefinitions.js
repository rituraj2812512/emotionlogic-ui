import * as Components from './_exportComponents.js'

export function registerBehavioralProfile(htmlTag = 'nms-dr-behavioral-profile') {
  customElements.define(htmlTag, Components.BehavioralProfile)
}
export function registerBehavioralProfileHeader(htmlTag = 'nms-dr-behavioral-profile-header') {
  customElements.define(htmlTag, Components.BehavioralProfileHeader)
}
export function registerBehavioralProfileAnticipation(htmlTag = 'nms-dr-behavioral-profile-anticipation') {
  customElements.define(htmlTag, Components.BehavioralProfileAnticipation)
}
export function registerBehavioralProfileConcentration(htmlTag = 'nms-dr-behavioral-profile-concentration') {
  customElements.define(htmlTag, Components.BehavioralProfileConcentration)
}
export function registerBehavioralProfileHesitation(htmlTag = 'nms-dr-behavioral-profile-hesitation') {
  customElements.define(htmlTag, Components.BehavioralProfileHesitation)
}