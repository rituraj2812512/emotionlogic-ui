import { defineCustomElement } from 'vue'

import _BehavioralProfile from './BehavioralProfile.ce.vue'
import _BehavioralProfileHeader from './BehavioralProfileHeader.ce.vue'
import _ChartAnticipation from './ChartAnticipation.ce.vue'
import _ChartConcentration from './ChartConcentration.ce.vue'
import _ChartHesitation from './ChartHesitation.ce.vue'

export const BehavioralProfile = defineCustomElement(_BehavioralProfile)
export const BehavioralProfileHeader = defineCustomElement(_BehavioralProfileHeader)
export const BehavioralProfileAnticipation = defineCustomElement(_ChartAnticipation)
export const BehavioralProfileConcentration = defineCustomElement(_ChartConcentration)
export const BehavioralProfileHesitation = defineCustomElement(_ChartHesitation)