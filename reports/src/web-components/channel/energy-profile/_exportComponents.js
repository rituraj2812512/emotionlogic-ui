import { defineCustomElement } from 'vue'

import _EnergyProfileHeader from './EnergyProfileHeader.ce.vue'
import _EnergyProfileLevels from './EnergyProfileLevels.ce.vue'
import _EnergyProfileExtreme from './EnergyProfileExtreme.ce.vue'
import _EnergyProfile from './EnergyProfile.ce.vue'

export const EnergyProfileHeader = defineCustomElement(_EnergyProfileHeader)
export const EnergyProfileLevels = defineCustomElement(_EnergyProfileLevels)
export const EnergyProfileExtreme = defineCustomElement(_EnergyProfileExtreme)
export const EnergyProfile = defineCustomElement(_EnergyProfile)