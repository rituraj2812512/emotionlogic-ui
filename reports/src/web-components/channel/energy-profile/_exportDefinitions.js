import * as Components from './_exportComponents.js'

export function registerEnergyProfileHeader(htmlTag = 'nms-dr-energy-profile-header') {
  customElements.define(htmlTag, Components.EnergyProfileHeader)
}
export function registerEnergyProfileLevels(htmlTag = 'nms-dr-energy-profile-levels') {
  customElements.define(htmlTag, Components.EnergyProfileLevels)
}
export function registerEnergyProfileExtreme(htmlTag = 'nms-dr-energy-profile-extreme') {
  customElements.define(htmlTag, Components.EnergyProfileExtreme)
}
export function registerEnergyProfile(htmlTag = 'nms-dr-energy-profile') {
  customElements.define(htmlTag, Components.EnergyProfile)
}