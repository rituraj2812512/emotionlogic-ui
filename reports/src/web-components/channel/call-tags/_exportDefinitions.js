import * as Components from './_exportComponents.js'

export function registerCallTags(htmlTag = 'nms-dr-call-tags') {
  customElements.define(htmlTag, Components.CallTags)
}