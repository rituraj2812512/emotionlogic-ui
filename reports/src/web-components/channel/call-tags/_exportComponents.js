import { defineCustomElement } from 'vue'

import _CallTags from './CallTags.ce.vue'

export const CallTags = defineCustomElement(_CallTags)