import { defineCustomElement } from 'vue'

import _ChannelCallTonePriority from './ChannelCallTonePriority.ce.vue'
import _ChannelCallMaxPriority from './ChannelCallMaxPriority.ce.vue'
import _ChannelCallFinalPriority from './ChannelCallFinalPriority.ce.vue'
import _ChannelCallPriority from './ChannelCallPriority.ce.vue'

export const ChannelCallTonePriority = defineCustomElement(_ChannelCallTonePriority)
export const ChannelCallMaxPriority = defineCustomElement(_ChannelCallMaxPriority)
export const ChannelCallFinalPriority = defineCustomElement(_ChannelCallFinalPriority)
export const ChannelCallPriority = defineCustomElement(_ChannelCallPriority)