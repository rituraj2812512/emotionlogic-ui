import * as Components from './_exportComponents.js'

export function registerChannelCallTonePriority(htmlTag = 'nms-dr-channel-call-tone-priority') {
  customElements.define(htmlTag, Components.ChannelCallTonePriority)
}
export function registerChannelCallMaxPriority(htmlTag = 'nms-dr-channel-call-max-priority') {
  customElements.define(htmlTag, Components.ChannelCallMaxPriority)
}
export function registerChannelCallFinalPriority(htmlTag = 'nms-dr-channel-call-final-priority') {
  customElements.define(htmlTag, Components.ChannelCallFinalPriority)
}
export function registerChannelCallPriority(htmlTag = 'nms-dr-channel-call-priority') {
  customElements.define(htmlTag, Components.ChannelCallPriority)
}