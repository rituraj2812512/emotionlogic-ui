import { defineCustomElement } from 'vue'

import _ChartAggression from './ChartAggression.ce.vue'
import _ChartJoy from './ChartJoy.ce.vue'
import _ChartSadness from './ChartSadness.ce.vue'
import _EmotionDistribution from './EmotionDistribution.ce.vue'
import _LineAnger from './LineAnger.ce.vue'
import _LineDissatisfaction from './LineDissatisfaction.ce.vue'
import _LineJoy from './LineJoy.ce.vue'
import _LineSad from './LineSad.ce.vue'
import _LineStress from './LineStress.ce.vue'
import _MoodProfile from './MoodProfile.ce.vue'
import _MoodProfileHeader from './MoodProfileHeader.ce.vue'

export const MoodProfileChartAggression = defineCustomElement(_ChartAggression)
export const MoodProfileChartJoy = defineCustomElement(_ChartJoy)
export const MoodProfileChartSadness = defineCustomElement(_ChartSadness)
export const MoodProfileEmotionDistribution = defineCustomElement(_EmotionDistribution)
export const MoodProfileLineAnger = defineCustomElement(_LineAnger)
export const MoodProfileLineDissatisfaction = defineCustomElement(_LineDissatisfaction)
export const MoodProfileLineJoy = defineCustomElement(_LineJoy)
export const MoodProfileLineSad = defineCustomElement(_LineSad)
export const MoodProfileLineStress = defineCustomElement(_LineStress)
export const MoodProfile = defineCustomElement(_MoodProfile)
export const MoodProfileHeader = defineCustomElement(_MoodProfileHeader)