import * as Components from './_exportComponents.js'

export function registerMoodProfileChartAggression(htmlTag = 'nms-dr-mood-profile-chart-aggression') {
  customElements.define(htmlTag, Components.MoodProfileChartAggression)
}
export function registerMoodProfileChartJoy(htmlTag = 'nms-dr-mood-profile-chart-joy') {
  customElements.define(htmlTag, Components.MoodProfileChartJoy)
}
export function registerMoodProfileChartSadness(htmlTag = 'nms-dr-mood-profile-chart-sadness') {
  customElements.define(htmlTag, Components.MoodProfileChartSadness)
}
export function registerMoodProfileEmotionDistribution(htmlTag = 'nms-dr-mood-profile-emotion-distribution') {
  customElements.define(htmlTag, Components.MoodProfileEmotionDistribution)
}
export function registerMoodProfileLineAnger(htmlTag = 'nms-dr-mood-profile-line-anger') {
  customElements.define(htmlTag, Components.MoodProfileLineAnger)
}
export function registerMoodProfileLineDissatisfaction(htmlTag = 'nms-dr-mood-profile-line-dissatisfaction') {
  customElements.define(htmlTag, Components.MoodProfileLineDissatisfaction)
}
export function registerMoodProfileLineJoy(htmlTag = 'nms-dr-mood-profile-line-joy') {
  customElements.define(htmlTag, Components.MoodProfileLineJoy)
}
export function registerMoodProfileLineSad(htmlTag = 'nms-dr-mood-profile-line-sad') {
  customElements.define(htmlTag, Components.MoodProfileLineSad)
}
export function registerMoodProfileLineStress(htmlTag = 'nms-dr-mood-profile-line-stress') {
  customElements.define(htmlTag, Components.MoodProfileLineStress)
}
export function registerMoodProfile(htmlTag = 'nms-dr-mood-profile') {
  customElements.define(htmlTag, Components.MoodProfile)
}
export function registerMoodProfileHeader(htmlTag = 'nms-dr-mood-profile-header') {
  customElements.define(htmlTag, Components.MoodProfileHeader)
}