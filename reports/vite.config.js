import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import svgLoader from 'vite-svg-loader'
import { createStyleImportPlugin } from 'vite-plugin-style-import'
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js'
import { fileURLToPath, URL } from 'url'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    cssInjectedByJsPlugin(),
    svgLoader({svgo: false}),
    createStyleImportPlugin({
      scss: {
        // Add the path to your global SCSS file here
        additionalData: `@import "./src/assets/styles/component.scss";`,
      },
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@use "@/assets/styles/component.scss" as *;',
      },
    },
  },
  define: {
    'process.env.NODE_ENV': '"production"',
  },
  build: {
    lib: {
      entry: './src/web-components/index.js',
      formats: ['es', 'cjs', 'umd'],
      name: 'webComponent',
      fileName: (format) => {
        if (format === 'es') return 'nms-dr-lib.js'
        if (format === 'cjs') return 'nms-dr-lib.cjs'
        if (format === 'umd') return 'nms-dr-lib.umd.cjs'
      },
    },
    sourcemap: true,
    // Reduce bloat from legacy polyfills.
    target: 'esnext',
    // Leave minification up to applications.
    minify: true,
  },
})
